import 'package:app_note/home.dart';
import 'package:app_note/note.dart';
import 'package:app_note/user.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class BottomBar extends StatefulWidget {
  var userId;
  BottomBar({Key? key, required this.userId}) : super(key: key);
  @override
  _BottomBarState createState() => _BottomBarState(this.userId);
}

class _BottomBarState extends State<BottomBar> {
  var userId;
  _BottomBarState(this.userId);

  int _selectedPage = 0;
  var _pageOptions;
  @override
  void initState() {
    super.initState();
    setState(() {
      late final Stream<QuerySnapshot> _profileStream = FirebaseFirestore
          .instance
          .collection('users')
          .where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .snapshots();
      print(FirebaseAuth.instance.currentUser!.uid);
    });
    _pageOptions = [Home(), Note(noteId: ''), UserWidget(id: '')];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: _pageOptions[_selectedPage],
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.pink.shade700,
            selectedItemColor: Colors.black,
            unselectedItemColor: Colors.white,
            currentIndex: _selectedPage,
            onTap: (int index) {
              setState(() {
                _selectedPage = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                  icon: Icon(IconData(0xe318, fontFamily: 'MaterialIcons')),
                  label: 'HOME'),
              BottomNavigationBarItem(
                  icon: Icon(IconData(0xf22f, fontFamily: 'MaterialIcons')),
                  label: 'NOTE'),
              BottomNavigationBarItem(
                  icon: Icon(IconData(0xee35, fontFamily: 'MaterialIcons')),
                  label: 'USER'),
            ]),
      ),
    );
  }
}
