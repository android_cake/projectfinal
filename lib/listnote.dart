import 'package:app_note/note.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Listnote extends StatefulWidget {
  Listnote({Key? key}) : super(key: key);

  @override
  _ListnoteState createState() => _ListnoteState();
}

class _ListnoteState extends State<Listnote> {
  final Stream<QuerySnapshot> _notesStream = FirebaseFirestore.instance
      .collection('notes')
      .where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
      .snapshots();
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');

  Future<void> deletenote(noteId) {
    return notes
        .doc(noteId)
        .delete()
        .then((value) => print("note Deleted"))
        .catchError((error) => print("Failed to delete note: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _notesStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;

            return Card(
                color: Colors.pink.shade100,
                margin: const EdgeInsets.all(16.0),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(data['title']),
                        IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: () async {
                              await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Note(noteId: document.id)));
                            })
                      ],
                    ),
                    Row(
                      children: [
                        Text(data['description']),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          icon: Icon(Icons.delete),
                          onPressed: () async {
                            await deletenote(document.id);
                          },
                        ),
                        Text(data['date']),
                      ],
                    ),
                  ]),
                ));
          }).toList(),
        );
      },
    );
  }
}
