import 'package:app_note/bottombar.dart';
import 'package:app_note/home.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Note extends StatefulWidget {
  String noteId;
  Note({Key? key, required this.noteId}) : super(key: key);

  @override
  _NoteState createState() => _NoteState(this.noteId);
}

class _NoteState extends State<Note> {
  final _formKey = GlobalKey<FormState>();
  String noteId;
  String title = '';
  String description = ' ';
  DateTime today = DateTime.now();
  String day = '';
  String mount = '';
  String year = '';
  String date = '';

  @override
  void initState() {
    super.initState();
    if (this.noteId.isNotEmpty) {
      notes.doc(this.noteId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          title = data['title'];
          description = data['description'];
          date = data['date'];
          titleController.text = title;
          descriptionController.text = description;
        }
      });
    }
  }

  TextEditingController dateController = new TextEditingController();
  TextEditingController titleController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  CollectionReference notes = FirebaseFirestore.instance.collection('notes');
  _NoteState(this.noteId);

  Future<void> addNote() {
    return notes
        .add({
          'userId': FirebaseAuth.instance.currentUser!.uid,
          'title': this.title,
          'description': this.description,
          'date': this.date
        })
        .then((value) => print("note Added"))
        .catchError((error) => print("Failed to add note: $error"));
  }

  Future<void> updateNote() {
    return notes
        .doc(this.noteId)
        .update({
          'userId': FirebaseAuth.instance.currentUser!.uid,
          'title': this.title,
          'description': this.description,
          'date': this.date
        })
        .then((value) => print("note Updated"))
        .catchError((error) => print("Failed to update note: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('NOTE'),
        centerTitle: true,
        backgroundColor: Colors.pink.shade200,
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                day = today.day.toString();
                mount = today.month.toString();
                year = today.year.toString();
                date = day + "-" + mount + "-" + year;
                if (noteId.isEmpty) {
                  await addNote();
                } else {
                  await updateNote();
                }

                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BottomBar(userId: '')),
                );
              }
            },
            icon: Icon(IconData(0xe550, fontFamily: 'MaterialIcons')),
          ),
        ],
      ),
      body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: titleController,
                  style: TextStyle(
                      fontFamily: 'Sans',
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 16),
                  decoration: InputDecoration(
                    hintText: "Title Note",
                  ),
                  onChanged: (value) {
                    setState(() {
                      title = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter the title';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: TextFormField(
                      style: TextStyle(
                          fontFamily: 'Sans',
                          fontWeight: FontWeight.normal,
                          color: Colors.black,
                          fontSize: 16),
                      controller: descriptionController,
                      decoration: InputDecoration(hintText: "Description Note"),
                      onChanged: (value) {
                        setState(() {
                          description = value;
                        });
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter the description';
                        }
                        return null;
                      },
                      maxLines: 10,
                      keyboardType: TextInputType.multiline),
                ),
              ],
            ),
          )),
    );
  }
}
