import 'package:app_note/userfrom.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserWidget extends StatefulWidget {
  var id;
  UserWidget({Key? key, required this.id}) : super(key: key);

  @override
  _UserWidgetState createState() => _UserWidgetState(this.id);
}

class _UserWidgetState extends State<UserWidget> {
  String name = '';
  int age = 0;
  String email = '';
  String docId = '';
  late final Stream<QuerySnapshot> _profileStream;
  @override
  void initState() {
    super.initState();
    setState(() {
      _profileStream = FirebaseFirestore.instance
          .collection('users')
          .where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid)
          .snapshots();
    });
  }

  CollectionReference users = FirebaseFirestore.instance.collection('users');
  var id;
  _UserWidgetState(this.id);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _profileStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }

          snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            name = data['name'];
            age = data['age'];
            email = data['email'];
            docId = document.id;
          }).toList();
          return Container(
            child: Builder(
                builder: (context) => Scaffold(
                    appBar: AppBar(
                      title: Text('USER'),
                      centerTitle: true,
                      backgroundColor: Colors.pink.shade200,
                      elevation: 0,
                    ),
                    body: Column(
                      children: <Widget>[
                        Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                  Colors.redAccent,
                                  Colors.pinkAccent
                                ])),
                            child: Container(
                              // width: 200.0,
                              // height: 40.0,
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                      Card(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 20.0, vertical: 5.0),
                                        clipBehavior: Clip.antiAlias,
                                        color: Colors.white,
                                        elevation: 5.0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Column(
                                              // leading: FlutterLogo(),
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.end,
                                                  children: [
                                                    IconButton(
                                                        icon: Icon(Icons.edit),
                                                        onPressed: () async {
                                                          await Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      UserFrom(
                                                                          userId:
                                                                              docId)));
                                                        })
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20.0,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text('Name'),
                                                    ),
                                                    Container(
                                                      child: Text("${name}"),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20.0,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text('Age'),
                                                    ),
                                                    Container(
                                                      child: Text("${age}"),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20.0,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      child: Text('Email'),
                                                    ),
                                                    Container(
                                                      child: Text("${email}"),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20.0,
                                                ),
                                              ]),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 40.0,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )),
                        SizedBox(
                          height: 40.0,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.red,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          child: Text(
                            'Log Out',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          onPressed: () async {
                            await FirebaseAuth.instance.signOut();
                          },
                          // child: Text('Sign In'),
                        )
                      ],
                    ))),
          );
        });
  }
}
